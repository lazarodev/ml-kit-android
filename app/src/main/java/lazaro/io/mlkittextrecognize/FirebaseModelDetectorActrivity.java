package lazaro.io.mlkittextrecognize;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.modeldownloader.CustomModel;
import com.google.firebase.ml.modeldownloader.CustomModelDownloadConditions;
import com.google.firebase.ml.modeldownloader.DownloadType;
import com.google.firebase.ml.modeldownloader.FirebaseModelDownloader;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.tensorflow.lite.Interpreter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class FirebaseModelDetectorActrivity extends AppCompatActivity {

    public final int CAMERA_CODE = 1100;
    private final static String CAMERA_TAG = "CAMERA:";
    private final static String MLKIT_TAG = "OBJECT_MLKIT";
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
    };
    private CameraPhoto cameraPhoto;
    private TextView tvCustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_model_detector_actrivity);

        if (!hasPermissions(this, PERMISSIONS)) {
            Log.w(CAMERA_TAG, "We can't start the camera yet");
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            Log.i(CAMERA_TAG, "We can start the camera.");
            initCamera();
        }

        tvCustom = findViewById(R.id.tv_custom_detection);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void initCamera() {
        cameraPhoto = new CameraPhoto(getApplicationContext());
    }

    public void takePictureFromCamera(View view) {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            startActivityForResult(cameraPhoto.takePhotoIntent(), CAMERA_CODE);
            cameraPhoto.addToGallery();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CODE) {
                String photoPath = cameraPhoto.getPhotoPath();
                Bitmap bitmap = null;
                try {
                    bitmap = ImageLoader.init().from(photoPath).requestSize(300, 300).getBitmap();
                    ExifInterface ei = new ExifInterface(photoPath);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);

                    File image = new File(photoPath);
                    Uri uri = Uri.fromFile(new File(photoPath));
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap picBitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
//                    picBitmap = Bitmap.createScaledBitmap(picBitmap, picBitmap.getWidth(), picBitmap.getHeight(), true);

//                    processImage(uri);
                    firebaseModelDetector(picBitmap);
//
//                    processBitmapWithCustomModel(bitmap);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void firebaseModelDetector(Bitmap yourInputImage) {

        CustomModelDownloadConditions conditions = new CustomModelDownloadConditions.Builder()
                .requireWifi()
                .build();
        FirebaseModelDownloader.getInstance()
                .getModel("shapes_detector", DownloadType.LOCAL_MODEL, conditions)
                .addOnSuccessListener(new OnSuccessListener<CustomModel>() {
                    @Override
                    public void onSuccess(CustomModel model) {
                        // Download complete. Depending on your app, you could enable
                        // the ML feature, or switch from the local model to the remote
                        // model, etc.

                        // The CustomModel object contains the local path of the model file,
                        // which you can use to instantiate a TensorFlow Lite interpreter.
                        File modelFile = model.getFile();
                        if (modelFile != null) {
                            Interpreter interpreter = new Interpreter(modelFile);
                            Bitmap bitmap = Bitmap.createScaledBitmap(yourInputImage, 300, 300, true);
//                            ByteBuffer input = ByteBuffer.allocateDirect(224 * 224 * 3 * 4).order(ByteOrder.nativeOrder());
                            ByteBuffer input = ByteBuffer.allocateDirect(300 * 300 * 3).order(ByteOrder.nativeOrder());
//                            for (int y = 0; y < 300; y++) {
//                                for (int x = 0; x < 300; x++) {
//                                    int px = bitmap.getPixel(x, y);
//
//                                    // Get channel values from the pixel value.
//                                    int r = Color.red(px);
//                                    int g = Color.green(px);
//                                    int b = Color.blue(px);
//
//                                    // Normalize channel values to [-1.0, 1.0]. This requirement depends
//                                    // on the model. For example, some models might require values to be
//                                    // normalized to the range [0.0, 1.0] instead.
//                                    float rf = (r - 127) / 255.0f;
//                                    float gf = (g - 127) / 255.0f;
//                                    float bf = (b - 127) / 255.0f;
//
//                                    input.putFloat(rf);
//                                    input.putFloat(gf);
//                                    input.putFloat(bf);
//                                }
//                            }

                            int bufferSize = 1000 * java.lang.Float.SIZE / java.lang.Byte.SIZE;

                            ByteBuffer modelOutput = ByteBuffer.allocateDirect(bufferSize).order(ByteOrder.nativeOrder());
                            interpreter.run(input, modelOutput);

                            modelOutput.rewind();
                            FloatBuffer probabilities = modelOutput.asFloatBuffer();
                            try {
                                BufferedReader reader = new BufferedReader(
                                        new InputStreamReader(getAssets().open("labelmap.txt")));

                                tvCustom.setText("");
                                for (int i = 0; i < probabilities.capacity(); i++) {
                                    String label = reader.readLine();
                                    float probability = probabilities.get(i);
                                    Log.i(MLKIT_TAG, String.format("%s: %1.4f", label, probability));

                                    if (label != null && !label.isEmpty()) {
                                        tvCustom.append("\n We detected a new shape in the photo as: " + label + ", with probabilty: " + probability + "\n");
                                    }
                                }
                            } catch (IOException e) {
                                // File not found?
                            }
                        }

                    }
                });
    }

}
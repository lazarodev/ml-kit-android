package lazaro.io.mlkittextrecognize;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.model.LocalModel;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.label.ImageLabel;
import com.google.mlkit.vision.label.ImageLabeler;
import com.google.mlkit.vision.label.ImageLabeling;
import com.google.mlkit.vision.label.custom.CustomImageLabelerOptions;
import com.google.mlkit.vision.label.defaults.ImageLabelerOptions;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class ImageLabelingActivity extends AppCompatActivity {

    public final int CAMERA_CODE = 1100;
    private final static String CAMERA_TAG = "CAMERA:";
    private final static String MLKIT_TAG = "OBJECT_MLKIT";
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
    };
    private CameraPhoto cameraPhoto;
    private TextView tvTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_labeling);

        tvTest = findViewById(R.id.tv_test_labeling);

        if (!hasPermissions(this, PERMISSIONS)) {
            Log.w(CAMERA_TAG, "We can't start the camera yet");
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            Log.i(CAMERA_TAG, "We can start the camera.");
            initCamera();
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void initCamera() {
        cameraPhoto = new CameraPhoto(getApplicationContext());
    }

    public void takePictureFromCamera(View view) {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            startActivityForResult(cameraPhoto.takePhotoIntent(), CAMERA_CODE);
            cameraPhoto.addToGallery();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CODE) {
                String photoPath = cameraPhoto.getPhotoPath();
                Bitmap bitmap = null;
                try {
                    bitmap = ImageLoader.init().from(photoPath).requestSize(300, 300).getBitmap();
                    ExifInterface ei = new ExifInterface(photoPath);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);

                    File image = new File(photoPath);
                    Uri uri = Uri.fromFile(new File(photoPath));
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap picBitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
//                    picBitmap = Bitmap.createScaledBitmap(picBitmap, picBitmap.getWidth(), picBitmap.getHeight(), true);

//                    processImage(uri);
                    processImageWithCustomModel(uri);
//
//                    processBitmapWithCustomModel(bitmap);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void processImage(Uri uri) {
        InputImage image;
        try {

//            LocalModel localModel =
//                    new LocalModel.Builder()
//                            .setAssetFilePath("detect.tflite")
//                            // or .setAbsoluteFilePath(absolute file path to model file)
//                            // or .setUri(URI to model file)
//                            .build();

            image = InputImage.fromFilePath(getApplicationContext(), uri);

//            CustomImageLabelerOptions customImageLabelerOptions =
//                    new CustomImageLabelerOptions.Builder(localModel)
//                            .setConfidenceThreshold(0.2f)
//                            .setMaxResultCount(5)
//                            .build();
//            ImageLabeler labeler = ImageLabeling.getClient(customImageLabelerOptions);

            // Or, to set the minimum confidence required:
            ImageLabelerOptions options =
                    new ImageLabelerOptions.Builder()
                            .setConfidenceThreshold(0.7f)
                            .build();
            ImageLabeler labeler = ImageLabeling.getClient(options);

            labeler.process(image)
                    .addOnSuccessListener(new OnSuccessListener<List<ImageLabel>>() {
                        @Override
                        public void onSuccess(List<ImageLabel> labels) {
                            Log.i(MLKIT_TAG, "Success!!");
                            // Task completed successfully
                            // ...
                            tvTest.setText("");
                            for (ImageLabel label : labels) {
                                String text = label.getText();
                                float confidence = label.getConfidence();
                                int index = label.getIndex();

                                Log.i(MLKIT_TAG, "Label text: " + text);
                                tvTest.append("\n" + text);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Task failed with an exception
                            // ...
                            Log.e(MLKIT_TAG, "ERROR: " + e.getMessage());
                        }
                    });


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processBitmapWithCustomModel(Bitmap bitmap) {

        LocalModel localModel =
                new LocalModel.Builder()
                        .setAssetFilePath("detect.tflite")
                        // or .setAbsoluteFilePath(absolute file path to model file)
                        // or .setUri(URI to model file)
                        .build();

        InputImage image = InputImage.fromBitmap(bitmap, 90);
        CustomImageLabelerOptions customImageLabelerOptions =
                new CustomImageLabelerOptions.Builder(localModel)
                        .setConfidenceThreshold(0.2f)
                        .setMaxResultCount(5)
                        .build();
        ImageLabeler labeler = ImageLabeling.getClient(customImageLabelerOptions);

        labeler.process(image)
                .addOnSuccessListener(new OnSuccessListener<List<ImageLabel>>() {
                    @Override
                    public void onSuccess(List<ImageLabel> labels) {
                        Log.i(MLKIT_TAG, "Sucess!! bitmap one");
                        // Task completed successfully
                        // ...
                        for (ImageLabel label : labels) {
                            String text = label.getText();
                            float confidence = label.getConfidence();
                            int index = label.getIndex();

                            tvTest.append("\n" + text);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
                        Log.e(MLKIT_TAG, "errormsg: " + e.getMessage());
                    }
                });
    }

    private void processImageWithCustomModel(Uri uri) {
        InputImage image;
        try {

            LocalModel localModel =
                    new LocalModel.Builder()
                            .setAssetFilePath("detect_metadata.tflite")
                            // or .setAbsoluteFilePath(absolute file path to model file)
                            // or .setUri(URI to model file)
                            .build();

            image = InputImage.fromFilePath(getApplicationContext(), uri);

            CustomImageLabelerOptions customImageLabelerOptions =
                    new CustomImageLabelerOptions.Builder(localModel)
                            .setConfidenceThreshold(0.2f)
                            .setMaxResultCount(5)
                            .build();
            ImageLabeler labeler = ImageLabeling.getClient(customImageLabelerOptions);

            // Or, to set the minimum confidence required:
            // ImageLabelerOptions options =
            //     new ImageLabelerOptions.Builder()
            //         .setConfidenceThreshold(0.7f)
            //         .build();
            // ImageLabeler labeler = ImageLabeling.getClient(options);

            labeler.process(image)
                    .addOnSuccessListener(new OnSuccessListener<List<ImageLabel>>() {
                        @Override
                        public void onSuccess(List<ImageLabel> labels) {
                            Log.i(MLKIT_TAG, "Success!!");
                            // Task completed successfully
                            // ...
                            tvTest.setText("");
                            for (ImageLabel label : labels) {
                                String text = label.getText();
                                float confidence = label.getConfidence();
                                int index = label.getIndex();

                                Log.i(MLKIT_TAG, "Label text: " + text);
                                tvTest.append("\n" + text);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Task failed with an exception
                            // ...
                            Log.e(MLKIT_TAG, "ERROR: " + e.getMessage());
                        }
                    });


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
package lazaro.io.mlkittextrecognize;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void goToTextRecognization(View view) {
        startActivity(new Intent(MenuActivity.this, MainActivity.class));
    }

    public void goToObjectDetection(View view) {
        startActivity(new Intent(MenuActivity.this, ObjectDetectionActivity.class));
    }

    public void goToImageLabeling(View view) {
        startActivity(new Intent(MenuActivity.this, ImageLabelingActivity.class));
    }

    public void goToModelDetector(View view) {
        startActivity(new Intent(MenuActivity.this, FirebaseModelDetectorActrivity.class));
    }

}